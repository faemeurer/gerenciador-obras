ARG BASE=openjdk:8-jdk-alpine
FROM $BASE

ENV LANG pt_BR.ISO-8859-1
ENV LOG_LEVEL INFO
ENV TZ America/Sao_Paulo

ARG JAR_FILE
ADD target/${JAR_FILE} /work/gerenciador-obras.jar

ENV APP_CONFIG " -Duser.language=pt \
                 -Duser.region=BR \
                 -Duser.timezone=${TZ} \
                 -Dapplication.name=gerenciador-obras"

WORKDIR /work

EXPOSE 9000

ENTRYPOINT java $(eval echo "$APP_CONFIG") $JAVA_OPTS  -jar /work/gerenciador-obras.jar
