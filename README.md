# Gerenciador de Obras

O software para gerenciamento de obras tem como objetivo prover serviços REST para cadastro e
consultas dos Responsáveis e Obras (pública e privada).

## 1. Linguagem, Recursos e Tecnologias
Para implementação do projeto foi utilizada linguagem Java 8, juntamente com o Spring Boot, H2
Database e o MAVEN para gerenciamento das dependências e compilação do projeto.

> A aplicação consta com os seguitens recursos/tecnologias:
- Estrutura do projeto, seguindo o padrão do próprio Spring Boot (Entity -> Repository → Service → Controller)
- Autenticação dos serviços com JWT e Spring Security
- Swagger (com autenticação)
- Recursos do Spring Data JPA (Paginação, Ordenação, Cache)
- Response padronizado dos serviços
- Mensagens do sistema através de arquivo properties
- Blindagem para os serviços POST e PUT com classes DTO
- Testes unitários com Junit 5
- H2 Database + console
- Roda com Docker

## 2. Pré-requisitos
- O JAVA 8 deve estar instalado. (Utlizado no desenvolvimento versão 1.8.0_161).
- O Maven deve estar instalado. (Utlizado no desenvolvimento versão 3.6.3)
- Conexão com internet para que o Maven possa baixar as dependências.
- Docker deve ser instalado (opcional)

## 3. Rodando a aplicação
Primeiro passo é acessar a raiz do projeto e compilar com o maven: 
> `mvn clean install`

Poderá ser utilizado as formas abaixo para iniciar os serviços:

- Com o Maven, utilizando o comando:
> `mvn spring-boot:run`

- Com o Java, utilizando o comando:
> `java -jar gerenciador-obras-1.0.0-SNAPSHOT.jar`

- Com o Docker, na raíz do projeto foi adicionado o arquivo Dockerfile(precisa do docker instalado na máquina), adicionei no pom um profile do Docker para gerar a imagem, utilizando o comando:
> `mvn clean package -Pdocker`

## 4. Acesso aos Serviços e a Base de dados
##### Acessando o Swagger no browser para ter acesso aos serviços:
> `http://<IP>:<PORTA>/docs`

Após acessar o swagger será possível visualizar todos os serviços disponívels, primeiramente será necessário realizar o login com o usuário abaixo:
````
Utilizar o serviço: /login
Usuário: admin
Senha: admin123
````

O retorno será um token JWT, que deverá ser utilizado para autenticar os demais serviços para utilização.

##### Acessando o console do H2

`http://<IP>:<PORTA>/h2`

Verificar no arquivo application.yml as informações da base de dados para fazer o login no console.

## 5. Os requisitos da implementação
Este projeto foi implementado conforme requisitos do documento abaixo: 
> [Requisitos.pdf](Requisitos.pdf)






