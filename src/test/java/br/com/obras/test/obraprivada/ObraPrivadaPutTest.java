package br.com.obras.test.obraprivada;

import br.com.obras.dto.ObraPrivadaDTO;
import br.com.obras.dto.ResponsavelInsertDTO;
import br.com.obras.enums.ZonaEnum;
import br.com.obras.exception.ObraResponse;
import br.com.obras.test.AbstractBootTest;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

public class ObraPrivadaPutTest extends AbstractBootTest {

    private static final String URL = V1_OBRAS + "/privadas";
    private static final String URL_RESP = V1_OBRAS + "/responsaveis";
    private static final String CD_RESP_1 = "RESP-OB-PRI-PUT-01";
    private static final String CD_RESP_2 = "RESP-OB-PRI-PUT-02";
    private static final String CPF_VALIDO_1 = "74242169027";
    private static final String CPF_VALIDO_2 = "56106973016";

    private int sequence = 55;

    /**
     * Teste para alteração de obra privada COM autenticação. Insere a obra privada e depois altera o responsável.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(1)
    public void testPutObraPrivada_thenStatus200() throws Exception {
        //insere o responsavel
        mvc.perform(postWithAuth(URL_RESP)
                .content(objectMapper.writeValueAsString(new ResponsavelInsertDTO(CD_RESP_1, "Responsável Put-Obra-Publica 1", CPF_VALIDO_1))))
                .andExpect(MockMvcResultMatchers.status().isOk());
        mvc.perform(postWithAuth(URL_RESP)
                .content(objectMapper.writeValueAsString(new ResponsavelInsertDTO(CD_RESP_2, "Responsável Put-Obra-Publica 2", CPF_VALIDO_2))))
                .andExpect(MockMvcResultMatchers.status().isOk());

        //insere a obra privada
        mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPrivadaDTO(50, "ObraPrivada Put 1", ZonaEnum.URBANA, 2000, CD_RESP_1))))
                .andExpect(MockMvcResultMatchers.status().isOk());

        //insere a obra privada
        mvc.perform(putWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPrivadaDTO(50, "ObraPrivada Put 1 (Alterado)", ZonaEnum.RURAL, 2500, CD_RESP_2))))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Teste para alteração de obra privada COM autenticação COM Número nao existente.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(2)
    public void testPutObraPrivada_numeroNaoExistente_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(putWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPrivadaDTO(sequence++, "ObraPrivada Put 2", ZonaEnum.URBANA, 2000, CD_RESP_1))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("obra.nao.existe"));
    }

    /**
     * Teste para alteração de obra privada COM autenticação COM Código de Responsável não informado.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(3)
    public void testPutObraPrivada_codigoResponsavelNaoInformado_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(putWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPrivadaDTO(50, "ObraPrivada Put 1 (Alterado)", ZonaEnum.URBANA, 2000, ""))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("responsavel.codigo.nao.informado"));
    }

    /**
     * Teste para alteração de obra privada COM autenticação COM Código de Responsável não existente.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(4)
    public void testPutObraPrivada_codigoResponsavelNaoExistente_thenStatus400() throws Exception {
        final String cdResp = "RRREEESSPP";
        final MvcResult result = mvc.perform(putWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPrivadaDTO(50, "ObraPrivada Put 1 (Alterado)", ZonaEnum.URBANA, 2000, cdResp))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("responsavel.nao.encontrado", cdResp));
    }

    /**
     * Teste para alteração de obra privada COM autenticação COM Campos obrigatórios não informados
     *
     * @throws Exception exceção
     */
    @Test
    @Order(5)
    public void testPutObraPrivada_camposObrigatoriosNaoPreenchidos_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(putWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPrivadaDTO(null, "", ZonaEnum.URBANA, 2000, ""))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        assertTrue(response.getErrors().stream().anyMatch(e ->
                e.getMessage().equals(messageService.getMessage("campo.obrigatorio"))));
    }

    /**
     * Teste para alteração de obra privada COM autenticação COM descricao inválida
     *
     * @throws Exception exceção
     */
    @Test
    @Order(6)
    public void testPutObraPrivada_descricaoInvalida_thenStatus400() throws Exception {
        final String descricao = "AKJDHAKJShDLAKhDAKshsahdasd80a9sda8 d90a d09ad09aua0usda09usdfsklçjflskjdflksjdflkjsdlkfjslkdfslkdjfslkjddn0a9und09aunsd0anu0sdAKJDHAKJShDLAKhDAKshsahdasd80a9sda8 d90a d09ad09aua0usda09udn0a9und09aunsd0anu0sdAKJDHAKJShDLAKhDAKshsahdasd80a9sda8 d90a d09ad09aua0usda09udn0a9und09aunsd0anu0sd";
        final MvcResult result = mvc.perform(putWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ObraPrivadaDTO(sequence++, descricao, ZonaEnum.RURAL, 1000, CD_RESP_1))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("obra.descricao.invalida"));
    }

    /**
     * Teste para alteração de obra privada COM autenticação COM zona não informada
     *
     * @throws Exception exceção
     */
    @Test
    @Order(7)
    public void testPutObraPrivada_zonaNaoInformada_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(putWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ObraPrivadaDTO(sequence++, "OBR-PRI-PUT", null, 1000, CD_RESP_1))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("obra.zona.obrigatorio"));
    }

    /**
     * Teste para alteração de obra privada SEM autenticação.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(8)
    public void testPutObraPrivada_semAutenticacao_thenStatus401() throws Exception {
        mvc.perform(putWithoutAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPrivadaDTO(sequence++, "ObraPrivada Put 1 (Alterado)", ZonaEnum.URBANA, 2000, CD_RESP_2))))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    /**
     * Teste para alteração de obra privada SEM permissao.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(9)
    public void testPutObraPrivada_semPermissao_thenStatus403() throws Exception {
        mvc.perform(defaultBuilder(MockMvcRequestBuilders.put(URL)
                .header("Authorization", getTokenUser1())))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

}