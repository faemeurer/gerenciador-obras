package br.com.obras.test.obraprivada;

import br.com.obras.dto.ObraPrivadaDTO;
import br.com.obras.dto.ResponsavelInsertDTO;
import br.com.obras.enums.ZonaEnum;
import br.com.obras.exception.ObraResponse;
import br.com.obras.test.AbstractBootTest;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ObraPrivadaDeleteTest extends AbstractBootTest {

    private static final String URL = V1_OBRAS + "/privadas";
    private static final String URL_RESP = V1_OBRAS + "/responsaveis";
    private static final String CD_RESP = "RESP-OBPRI-DELETE-01";

    /**
     * Teste para exclusão de obra privada COM autenticação COM Código existente.
     *
     * @throws Exception
     */
    @Test
    public void testDeleteObraPrivada_thenStatus200() throws Exception {
        //insere o responsavel
        mvc.perform(postWithAuth(URL_RESP)
                .content(objectMapper.writeValueAsString(new ResponsavelInsertDTO(CD_RESP, "Responsável 1", "24574560029"))))
                .andExpect(MockMvcResultMatchers.status().isOk());

        //insere a obra privada
        mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPrivadaDTO(30, "ObraPrivada Delete 1", ZonaEnum.RURAL, 100, CD_RESP))))
                .andExpect(MockMvcResultMatchers.status().isOk());

        //testa a exclusão
        mvc.perform(deleteWithAuth(URL + "/" + 30))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Teste para exclusão de obra privada COM autenticação COM Código inexistente.
     *
     * @throws Exception
     */
    @Test
    public void testDeleteObraPrivada_codigoInexistente_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(deleteWithAuth(URL + "/" + 31))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("obra.nao.existe"));
    }

    /**
     * Teste para exclusão de obra privada SEM autenticação.
     *
     * @throws Exception
     */
    @Test
    public void testDeleteObraPrivada_semAutenticacao_thenStatus401() throws Exception {
        mvc.perform(deleteWithoutAuth(URL))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    /**
     * Teste para exclusão de obra privada SEM permissao.
     *
     * @throws Exception
     */
    @Test
    public void testDeleteObraPrivada_semPermissao_thenStatus403() throws Exception {
        mvc.perform(defaultBuilder(MockMvcRequestBuilders.delete(URL)
                .header("Authorization", getTokenUser1())))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

}