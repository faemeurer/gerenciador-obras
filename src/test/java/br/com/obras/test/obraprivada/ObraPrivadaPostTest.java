package br.com.obras.test.obraprivada;

import br.com.obras.dto.ObraPrivadaDTO;
import br.com.obras.dto.ResponsavelInsertDTO;
import br.com.obras.enums.ZonaEnum;
import br.com.obras.exception.ObraResponse;
import br.com.obras.test.AbstractBootTest;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

public class ObraPrivadaPostTest extends AbstractBootTest {

    private static final String URL = V1_OBRAS + "/privadas";
    private static final String URL_RESP = V1_OBRAS + "/responsaveis";
    private static final String CD_RESP = "RESP-OB-PRI-POST-01";
    private static final String CPF_VALIDO = "54361453020";

    private int sequence = 45;

    /**
     * Teste para inclusão de obra privada COM autenticação.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(1)
    public void testPostObraPrivada_thenStatus200() throws Exception {
        //insere o responsavel
        mvc.perform(postWithAuth(URL_RESP)
                .content(objectMapper.writeValueAsString(new ResponsavelInsertDTO(CD_RESP, "Responsável Post-Obra-Publica 1", CPF_VALIDO))))
                .andExpect(MockMvcResultMatchers.status().isOk());

        //insere a obra privada
        mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPrivadaDTO(40, "ObraPrivada Post 1", ZonaEnum.RURAL, 1000, CD_RESP))))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Teste para inclusão de obra privada COM autenticação COM Número existente.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(2)
    public void testPostObraPrivada_numeroExistente_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPrivadaDTO(40, "ObraPrivada Post 2", ZonaEnum.RURAL, 1000, CD_RESP))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("obra.numero.existente"));
    }

    /**
     * Teste para inclusão de obra privada COM autenticação COM Código de Responsável não informado.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(3)
    public void testPostObraPrivada_codigoResponsavelNaoInformado_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPrivadaDTO(sequence++, "ObraPrivada Post 3", ZonaEnum.RURAL, 1000, ""))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("responsavel.codigo.nao.informado"));
    }

    /**
     * Teste para inclusão de obra privada COM autenticação COM Código de Responsável não existente.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(4)
    public void testPostObraPrivada_codigoResponsavelNaoExistente_thenStatus400() throws Exception {
        final String cdResp = "RREESSPP";
        final MvcResult result = mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPrivadaDTO(sequence++, "ObraPrivada Post 4", ZonaEnum.RURAL, 1000, cdResp))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("responsavel.nao.encontrado", cdResp));
    }

    /**
     * Teste para inclusão de obra privada COM autenticação COM Campos obrigatórios não informados
     *
     * @throws Exception exceção
     */
    @Test
    @Order(5)
    public void testPostObraPrivada_camposObrigatoriosNaoPreenchidos_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPrivadaDTO(null, "", ZonaEnum.RURAL, 1000, ""))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        assertTrue(response.getErrors().stream().anyMatch(e ->
                e.getMessage().equals(messageService.getMessage("campo.obrigatorio"))));
    }

    /**
     * Teste para inclusão de obra privada COM autenticação COM descricao inválida
     *
     * @throws Exception exceção
     */
    @Test
    @Order(6)
    public void testPostObraPrivada_descricaoInvalida_thenStatus400() throws Exception {
        final String descricao = "AKJDHAKJShDLAKhDAKshsahdasd80a9sda8 d90a d09ad09aua0usda09usdfsklçjflskjdflksjdflkjsdlkfjslkdfslkdjfslkjddn0a9und09aunsd0anu0sdAKJDHAKJShDLAKhDAKshsahdasd80a9sda8 d90a d09ad09aua0usda09udn0a9und09aunsd0anu0sdAKJDHAKJShDLAKhDAKshsahdasd80a9sda8 d90a d09ad09aua0usda09udn0a9und09aunsd0anu0sd";
        final MvcResult result = mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ObraPrivadaDTO(sequence++, descricao, ZonaEnum.RURAL, 1000, CD_RESP))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("obra.descricao.invalida"));
    }

    /**
     * Teste para inclusão de obra privada COM autenticação COM zona não informada
     *
     * @throws Exception exceção
     */
    @Test
    @Order(7)
    public void testPostObraPrivada_zonaNaoInformada_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ObraPrivadaDTO(sequence++, "OBR-PRI-POST", null, 1000, CD_RESP))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("obra.zona.obrigatorio"));
    }

    /**
     * Teste para inclusão de obra privada SEM autenticação.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(8)
    public void testPostObraPrivada_semAutenticacao_thenStatus401() throws Exception {
        mvc.perform(postWithoutAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPrivadaDTO(sequence++, "ObraPrivada Post 5", ZonaEnum.RURAL, 1000, CD_RESP))))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    /**
     * Teste para inclusão de obra privada SEM permissao.
     *
     * @throws Exception
     */
    @Test
    @Order(9)
    public void testPostObraPrivada_semPermissao_thenStatus403() throws Exception {
        mvc.perform(defaultBuilder(MockMvcRequestBuilders.post(URL)
                .header("Authorization", getTokenUser1())))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

}