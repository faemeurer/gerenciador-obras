package br.com.obras.test;

import br.com.obras.dto.UserDTO;
import br.com.obras.exception.ObraResponse;
import br.com.obras.service.MessageService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public abstract class AbstractBootTest {

    private static final String AUTHORIZATION = "Authorization";
    protected static final String V1 = "/v1";
    protected static final String V1_OBRAS = V1 + "/obras";

    private String tokenAdmin;
    private String tokenUser1;

    @Autowired
    protected MockMvc mvc;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected MessageService messageService;

    public String getTokenAdmin() {
        return tokenAdmin;
    }

    public String getTokenUser1() {
        return tokenUser1;
    }

    @BeforeAll
    public void login() throws Exception {
        //-----------------------
        final MvcResult result1 = mvc.perform(MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(new UserDTO("admin", "admin123"))))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        tokenAdmin = getObraResponse(result1).getData().toString();
        //-----------------------
        final MvcResult result2 = mvc.perform(MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(new UserDTO("user1", "user123"))))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        tokenUser1 = getObraResponse(result2).getData().toString();
    }

    protected ObraResponse getObraResponse(MvcResult result) throws UnsupportedEncodingException, JsonProcessingException {
        return objectMapper.readValue(result.getResponse().getContentAsString(Charset.forName("UTF-8")), ObraResponse.class);
    }

    //com autenticação
    protected MockHttpServletRequestBuilder getWithAuth(String url) {
        return withAuth(MockMvcRequestBuilders.get(url));
    }

    protected MockHttpServletRequestBuilder postWithAuth(String url) {
        return withAuth(MockMvcRequestBuilders.post(url));
    }

    protected MockHttpServletRequestBuilder putWithAuth(String url) {
        return withAuth(MockMvcRequestBuilders.put(url));
    }

    protected MockHttpServletRequestBuilder deleteWithAuth(String url) {
        return withAuth(MockMvcRequestBuilders.delete(url));
    }

    //sem autenticação
    protected MockHttpServletRequestBuilder getWithoutAuth(String url) {
        return withoutAuth(MockMvcRequestBuilders.get(url));
    }

    protected MockHttpServletRequestBuilder postWithoutAuth(String url) {
        return withoutAuth(MockMvcRequestBuilders.post(url));
    }

    protected MockHttpServletRequestBuilder putWithoutAuth(String url) {
        return withoutAuth(MockMvcRequestBuilders.put(url));
    }

    protected MockHttpServletRequestBuilder deleteWithoutAuth(String url) {
        return withoutAuth(MockMvcRequestBuilders.delete(url));
    }

    /**
     * Monta a requisição COM autenticação
     *
     * @param builder {@link MockHttpServletRequestBuilder}
     * @return {@link MockHttpServletRequestBuilder}
     */
    private MockHttpServletRequestBuilder withAuth(MockHttpServletRequestBuilder builder) {
        return defaultBuilder(builder.header(AUTHORIZATION, getTokenAdmin()));
    }

    /**
     * Monta a requisição SEM autenticação
     *
     * @param builder {@link MockHttpServletRequestBuilder}
     * @return {@link MockHttpServletRequestBuilder}
     */
    private MockHttpServletRequestBuilder withoutAuth(MockHttpServletRequestBuilder builder) {
        return defaultBuilder(builder);
    }

    /**
     * Monta a requisição padrão.
     *
     * @param builder {@link MockHttpServletRequestBuilder}
     * @return {@link MockHttpServletRequestBuilder}
     */
    protected MockHttpServletRequestBuilder defaultBuilder(MockHttpServletRequestBuilder builder) {
        return builder.contentType(MediaType.APPLICATION_JSON_VALUE);
    }

}