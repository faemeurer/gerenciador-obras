package br.com.obras.test.responsavel;

import br.com.obras.dto.ResponsavelInsertDTO;
import br.com.obras.exception.ObraResponse;
import br.com.obras.test.AbstractBootTest;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

public class ResponsavelDeleteTest extends AbstractBootTest {

    private static final String URL = V1_OBRAS + "/responsaveis";

    /**
     * Teste para exclusão de responsável COM autenticação COM Código existente.
     *
     * @throws Exception
     */
    @Test
    public void testDeleteResponsavel_thenStatus200() throws Exception {
        final String codigo = "RESP-DELETE-1";

        //insere para depois testar a exclusão
        mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ResponsavelInsertDTO(codigo, "Responsável 1", "69608183030"))))
                .andExpect(MockMvcResultMatchers.status().isOk());

        //testa a exclusão
        mvc.perform(deleteWithAuth(URL + "/" + codigo))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Teste para exclusão de responsável COM autenticação COM Código inexistente.
     *
     * @throws Exception
     */
    @Test
    public void testDeleteResponsavel_codigoInexistente_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(deleteWithAuth(URL + "/RESP-DELETE-2"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("responsavel.nao.existe"));
    }

    /**
     * Teste para exclusão de responsável COM autenticação COM Código inválido.
     *
     * @throws Exception
     */
    @Test
    public void testDeleteResponsavel_codigoInvalido_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(deleteWithAuth(URL + "/ASKJhDKAJSDKJAHSDKHASKJhDAKJHDKJAShDKJ"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertTrue(messageError.contains(messageService.getMessage("responsavel.codigo.invalido")));
    }

    /**
     * Teste para exclusão de responsável SEM autenticação.
     *
     * @throws Exception
     */
    @Test
    public void testDeleteResponsavel_semAutenticacao_thenStatus401() throws Exception {
        mvc.perform(deleteWithoutAuth(URL))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    /**
     * Teste para exclusão de responsável SEM permissao.
     *
     * @throws Exception
     */
    @Test
    public void testDeleteResponsavel_semPermissao_thenStatus403() throws Exception {
        mvc.perform(defaultBuilder(MockMvcRequestBuilders.delete(URL)
                .header("Authorization", getTokenUser1())))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

}