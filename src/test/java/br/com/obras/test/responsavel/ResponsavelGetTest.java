package br.com.obras.test.responsavel;

import br.com.obras.test.AbstractBootTest;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

public class ResponsavelGetTest extends AbstractBootTest {

    private static final String URL = V1_OBRAS + "/responsaveis";

    /**
     * Teste para lista de responsáveis COM autenticação.
     *
     * @throws Exception
     */
    @Test
    public void testListaResponsaveis_thenStatus200() throws Exception {
        mvc.perform(getWithAuth(URL))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }

    /**
     * Teste para lista de responsáveis SEM autenticação.
     *
     * @throws Exception
     */
    @Test
    public void testListaResponsaveis_semAutenticacao_thenStatus401() throws Exception {
        mvc.perform(getWithoutAuth(URL))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized())
                .andReturn();
    }

    /**
     * Teste para lista de responsáveis COM autenticação utilizando paginação e ordenação.
     *
     * @throws Exception
     */
    @Test
    public void testListaResponsaveis_paginadoOrdenado_thenStatus200() throws Exception {
        mvc.perform(getWithAuth(URL + "/?page=1&size=10&sort=nome"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }

}