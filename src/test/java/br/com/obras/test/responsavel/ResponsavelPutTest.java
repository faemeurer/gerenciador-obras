package br.com.obras.test.responsavel;

import br.com.obras.dto.ResponsavelDTO;
import br.com.obras.dto.ResponsavelInsertDTO;
import br.com.obras.exception.ObraResponse;
import br.com.obras.test.AbstractBootTest;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

public class ResponsavelPutTest extends AbstractBootTest {

    private static final String URL = V1_OBRAS + "/responsaveis";
    private static final String CODIGO = "RESP-PUT-1";
    private static final String CPF_VALIDO = "27401058006";

    /**
     * Teste para alteração de responsável COM autenticação.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(1)
    public void testPutResponsavel_thenStatus200() throws Exception {
        mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ResponsavelInsertDTO(CODIGO, "Responsável 1", CPF_VALIDO))))
                .andExpect(MockMvcResultMatchers.status().isOk());

        mvc.perform(putWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ResponsavelDTO(CODIGO, "Responsável 1 (Alterado)"))))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Teste para alteração de responsável COM autenticação COM Responsável nao cadastrado.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(2)
    public void testPutResponsavel_responsavelNaoCadastrado_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(putWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ResponsavelDTO("RESP-PUT-2", "Responsável 2 (Alterado)"))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("responsavel.nao.cadastrado"));
    }

    /**
     * Teste para alteração de responsável COM autenticação COM código inválido.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(3)
    public void testPutResponsavel_codigoInvalido_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(putWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ResponsavelDTO("ABCDEFGHIJKLMNOPQRSTUV", "Responsável 2 (Alterado)"))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("responsavel.codigo.invalido"));
    }

    /**
     * Teste para alteração de responsável COM autenticação COM Campos obrigatórios não informados
     *
     * @throws Exception exceção
     */
    @Test
    @Order(4)
    public void testPutResponsavel_camposObrigatoriosNaoPreenchidos_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(putWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ResponsavelDTO("", ""))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        assertTrue(response.getErrors().stream().anyMatch(e ->
                e.getMessage().equals(messageService.getMessage("campo.obrigatorio"))));
    }

    /**
     * Teste para alterção de responsável COM autenticação COM nome inválido
     *
     * @throws Exception exceção
     */
    @Test
    @Order(5)
    public void testPutResponsavel_nomeInvalido_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(putWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ResponsavelInsertDTO("RESP-PUT-3",
                        "ALKSJda9sd09a88sd07a0dUANs0duN)A(Sud)(ASud0A(Sud)AS(d)A(sd0as98(As098dn)AS(8dn09NAS)98dnALKSJda9sd09a88sd07a0dUANs0duN)A(Sud)(ASud0A(Sud)AS(d)A(sd0as98(As098dn)AS(8dn09NAS)98dnALKSJda9sd09a88sd07a0dUANs0duN)A(Sud)(ASud0A(Sud)AS(d)A(sd0as98(As098dn)AS(8dn09NAS)98dn",
                        CPF_VALIDO))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("responsavel.nome.invalido"));
    }

    /**
     * Teste para alteração de responsável SEM autenticação.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(6)
    public void testPutResponsavel_semAutenticacao_thenStatus401() throws Exception {
        mvc.perform(putWithoutAuth(URL)
                .content(objectMapper.writeValueAsString(new ResponsavelDTO("RESP-PUT-4", "Responsável 4 (Alterado)"))))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    /**
     * Teste para alteração de responsável SEM permissao.
     *
     * @throws Exception
     */
    @Test
    @Order(7)
    public void testPutResponsavel_semPermissao_thenStatus403() throws Exception {
        mvc.perform(defaultBuilder(MockMvcRequestBuilders.put(URL)
                .header("Authorization", getTokenUser1())))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

}