package br.com.obras.test.responsavel;

import br.com.obras.dto.ResponsavelInsertDTO;
import br.com.obras.exception.ObraResponse;
import br.com.obras.test.AbstractBootTest;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

public class ResponsavelPostTest extends AbstractBootTest {

    private static final String URL = V1_OBRAS + "/responsaveis";
    private static final String CODIGO = "RESP-POST-1";
    private static final String CPF_VALIDO = "69608183030";
    private static final String CPF_INVALIDO = "696081830301";

    /**
     * Teste para inclusão de responsável COM autenticação.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(1)
    public void testPostResponsavel_thenStatus200() throws Exception {
        mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ResponsavelInsertDTO(CODIGO, "Responsável 1", CPF_VALIDO))))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Teste para inclusão de responsável COM autenticação COM Cpf inválido.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(2)
    public void testPostResponsavel_cpfInvalido_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ResponsavelInsertDTO("RESP-POST-2", "Responsável 2", CPF_INVALIDO))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        assertTrue(response.getErrors().stream().anyMatch(e ->
                e.getMessage().equals(messageService.getMessage("responsavel.cpf.invalido"))));
    }

    /**
     * Teste para inclusão de responsável COM autenticação COM Código de Responsável existente na tabela.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(3)
    public void testPostResponsavel_codigoResponsavelExistente_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ResponsavelInsertDTO(CODIGO, "Responsável 1", CPF_VALIDO))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("responsavel.codigo.existente"));
    }

    /**
     * Teste para alteração de responsável COM autenticação COM código inválido.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(4)
    public void testPostResponsavel_codigoInvalido_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ResponsavelInsertDTO("ABCDEFGHIJKLMNOPQRSTUV", "Responsável 1", CPF_VALIDO))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("responsavel.codigo.invalido"));
    }

    /**
     * Teste para inclusão de responsável COM autenticação COM Cpf existente na tabela.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(5)
    public void testPostResponsavel_cpfExistente_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ResponsavelInsertDTO("RESP-POST-3", "Responsável 3", CPF_VALIDO))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("responsavel.cpf.existente"));
    }

    /**
     * Teste para inclusão de responsável COM autenticação COM Campos obrigatórios não informados
     *
     * @throws Exception exceção
     */
    @Test
    @Order(6)
    public void testPostResponsavel_camposObrigatoriosNaoPreenchidos_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ResponsavelInsertDTO("", "", ""))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        assertTrue(response.getErrors().stream().anyMatch(e ->
                e.getMessage().equals(messageService.getMessage("campo.obrigatorio"))));
    }

    /**
     * Teste para inclusão de responsável COM autenticação COM nome inválido
     *
     * @throws Exception exceção
     */
    @Test
    @Order(7)
    public void testPostResponsavel_nomeInvalido_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ResponsavelInsertDTO("RESP-POST-4",
                        "ALKSJda9sd09a88sd07a0dUANs0duN)A(Sud)(ASud0A(Sud)AS(d)A(sd0as98(As098dn)AS(8dn09NAS)98dnALKSJda9sd09a88sd07a0dUANs0duN)A(Sud)(ASud0A(Sud)AS(d)A(sd0as98(As098dn)AS(8dn09NAS)98dnALKSJda9sd09a88sd07a0dUANs0duN)A(Sud)(ASud0A(Sud)AS(d)A(sd0as98(As098dn)AS(8dn09NAS)98dn",
                        CPF_VALIDO))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("responsavel.nome.invalido"));
    }

    /**
     * Teste para inclusão de responsável SEM autenticação.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(8)
    public void testPostResponsavel_semAutenticacao_thenStatus200() throws Exception {
        mvc.perform(postWithoutAuth(URL)
                .content(objectMapper.writeValueAsString(new ResponsavelInsertDTO("RESP-POST-5", "Responsável 4", CPF_VALIDO))))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    /**
     * Teste para inclusão de responsável SEM permissao.
     *
     * @throws Exception
     */
    @Test
    @Order(9)
    public void testPostResponsavel_semPermissao_thenStatus403() throws Exception {
        mvc.perform(defaultBuilder(MockMvcRequestBuilders.post(URL)
                .header("Authorization", getTokenUser1())))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

}