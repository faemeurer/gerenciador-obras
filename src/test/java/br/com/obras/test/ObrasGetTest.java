package br.com.obras.test;

import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

public class ObrasGetTest extends AbstractBootTest {

    private static final String URL_PUBLICAS = V1_OBRAS + "/publicas";
    private static final String URL_PRIVADAS = V1_OBRAS + "/privadas";
    private static final String URL_RESPONSAVEIS = V1_OBRAS + "/responsaveis";

    @Test
    public void testListaObras_thenStatus200() throws Exception {
        mvc.perform(getWithAuth(V1_OBRAS))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }
    @Test
    public void testListaObras_paginado_thenStatus200() throws Exception {
        mvc.perform(getWithAuth(V1_OBRAS + "/?page=1&size=10"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }
    @Test
    public void testListaObras_naoAutenticado_thenStatus401() throws Exception {
        mvc.perform(getWithoutAuth(V1_OBRAS))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    //------------------------

    @Test
    public void testListaObrasPublicas_thenStatus200() throws Exception {
        mvc.perform(getWithAuth(URL_PUBLICAS))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }
    @Test
    public void testListaObrasPublicas_paginado_thenStatus200() throws Exception {
        mvc.perform(getWithAuth(URL_PUBLICAS + "/?page=1&size=10"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }
    @Test
    public void testListaObrasPublicas_naoAutenticado_thenStatus401() throws Exception {
        mvc.perform(getWithoutAuth(URL_PUBLICAS))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    //------------------------

    @Test
    public void testListaObrasPrivadas_thenStatus200() throws Exception {
        mvc.perform(getWithAuth(URL_PRIVADAS))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }
    @Test
    public void testListaObrasPrivadas_paginado_thenStatus200() throws Exception {
        mvc.perform(getWithAuth(URL_PRIVADAS + "/?page=1&size=10"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }
    @Test
    public void testListaObrasPrivadas_naoAutenticado_thenStatus401() throws Exception {
        mvc.perform(getWithoutAuth(URL_PRIVADAS))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    //------------------------

    @Test
    public void testListaObrasPorResponsavel_thenStatus200() throws Exception {
        mvc.perform(getWithAuth(URL_RESPONSAVEIS + "/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }
    @Test
    public void testListaObrasPorResponsavel_paginado_thenStatus200() throws Exception {
        mvc.perform(getWithAuth(URL_RESPONSAVEIS + "/1/?page=1&size=10"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }
    @Test
    public void testListaObrasPorResponsavel_naoAutenticado_thenStatus401() throws Exception {
        mvc.perform(getWithoutAuth(URL_RESPONSAVEIS + "/1"))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

}