package br.com.obras.test.login;

import br.com.obras.dto.UserDTO;
import br.com.obras.exception.ObraResponse;
import br.com.obras.test.AbstractBootTest;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class LoginTest extends AbstractBootTest {

    private static final String URL = "/login";

    /**
     * Realiza o teste de login para o usuário ADMIN.
     *
     * @throws Exception
     */
    @Test
    public void testLogin_usuarioAdmin_thenStatus200() throws Exception {
        mvc.perform(postWithoutAuth(URL)
                .content(objectMapper.writeValueAsString(new UserDTO("admin", "admin123"))))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Realiza o teste de login com usuário inválido.
     *
     * @throws Exception
     */
    @Test
    public void testLogin_usuarioInvalido_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(postWithoutAuth(URL)
                .content(objectMapper.writeValueAsString(new UserDTO("invalido", "invalido"))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("login.invalido"));
    }

}