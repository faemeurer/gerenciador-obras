package br.com.obras.test.obrapublica;

import br.com.obras.dto.ObraPublicaDTO;
import br.com.obras.dto.ResponsavelInsertDTO;
import br.com.obras.exception.ObraResponse;
import br.com.obras.test.AbstractBootTest;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ObraPublicaDeleteTest extends AbstractBootTest {

    private static final String URL = V1_OBRAS + "/publicas";
    private static final String URL_RESP = V1_OBRAS + "/responsaveis";
    private static final String CD_RESP = "RESP-OBPUB-DELETE-01";

    /**
     * Teste para exclusão de obra pública COM autenticação COM Código existente.
     *
     * @throws Exception
     */
    @Test
    public void testDeleteObraPublica_thenStatus200() throws Exception {
        //insere o responsavel
        mvc.perform(postWithAuth(URL_RESP)
                .content(objectMapper.writeValueAsString(new ResponsavelInsertDTO(CD_RESP, "Responsável 1", "83573523080"))))
                .andExpect(MockMvcResultMatchers.status().isOk());

        //insere a obra pública
        mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPublicaDTO(1000, "ObraPublica Delete 1", LocalDate.now(), null, CD_RESP))))
                .andExpect(MockMvcResultMatchers.status().isOk());

        //testa a exclusão
        mvc.perform(deleteWithAuth(URL + "/" + 1))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Teste para exclusão de obra pública COM autenticação COM Código inexistente.
     *
     * @throws Exception
     */
    @Test
    public void testDeleteObraPublica_codigoInexistente_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(deleteWithAuth(URL + "/" + 2000))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("obra.nao.existe"));
    }

    /**
     * Teste para exclusão de obra pública SEM autenticação.
     *
     * @throws Exception
     */
    @Test
    public void testDeleteObraPublica_semAutenticacao_thenStatus401() throws Exception {
        mvc.perform(deleteWithoutAuth(URL))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    /**
     * Teste para exclusão de obra pública SEM permissao.
     *
     * @throws Exception
     */
    @Test
    public void testDeleteObraPublica_semPermissao_thenStatus403() throws Exception {
        mvc.perform(defaultBuilder(MockMvcRequestBuilders.delete(URL)
                .header("Authorization", getTokenUser1())))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

}