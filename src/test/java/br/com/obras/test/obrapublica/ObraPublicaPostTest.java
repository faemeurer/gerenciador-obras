package br.com.obras.test.obrapublica;

import br.com.obras.dto.ObraPublicaDTO;
import br.com.obras.dto.ResponsavelInsertDTO;
import br.com.obras.exception.ObraResponse;
import br.com.obras.test.AbstractBootTest;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class ObraPublicaPostTest extends AbstractBootTest {

    private static final String URL = V1_OBRAS + "/publicas";
    private static final String URL_RESP = V1_OBRAS + "/responsaveis";
    private static final String CD_RESP = "RESP-OB-PUB-POST-01";
    private static final String CPF_VALIDO = "29655100014";

    private int sequence = 15;

    /**
     * Teste para inclusão de obra pública COM autenticação.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(1)
    public void testPostObraPublica_thenStatus200() throws Exception {
        //insere o responsavel
        mvc.perform(postWithAuth(URL_RESP)
                .content(objectMapper.writeValueAsString(new ResponsavelInsertDTO(CD_RESP, "Responsável Post-Obra-Publica 1", CPF_VALIDO))))
                .andExpect(MockMvcResultMatchers.status().isOk());

        //insere a obra pública
        mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPublicaDTO(10, "ObraPublica Post 1", LocalDate.now(), null, CD_RESP))))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Teste para inclusão de obra pública COM autenticação COM Número existente.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(2)
    public void testPostObraPublica_comNumeroExistente_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPublicaDTO(10, "ObraPublica Post 2", LocalDate.now(), null, CD_RESP))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("obra.numero.existente"));
    }

    /**
     * Teste para inclusão de obra pública COM autenticação COM Código de Responsável não informado.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(3)
    public void testPostObraPublica_codigoResponsavelNaoInformado_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPublicaDTO(sequence++, "ObraPublica Post 3", LocalDate.now(), null, ""))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("responsavel.codigo.nao.informado"));
    }

    /**
     * Teste para inclusão de obra pública COM autenticação COM Código de Responsável não existente.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(4)
    public void testPostObraPublica_codigoResponsavelNaoExistente_thenStatus400() throws Exception {
        final String cdResp = "RRREESSPP";
        final MvcResult result = mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPublicaDTO(sequence++, "ObraPublica Post 4", LocalDate.now(), null, cdResp))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("responsavel.nao.encontrado", cdResp));
    }

    /**
     * Teste para inclusão de obra pública COM autenticação COM Campos obrigatórios não informados
     *
     * @throws Exception exceção
     */
    @Test
    @Order(5)
    public void testPostObraPublica_camposObrigatoriosNaoPreenchidos_thenStatus400() throws Exception {
        final MvcResult result = mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPublicaDTO(null, "", LocalDate.now(), null, ""))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        assertTrue(response.getErrors().stream().anyMatch(e ->
                e.getMessage().equals(messageService.getMessage("campo.obrigatorio"))));
    }

    /**
     * Teste para inclusão de obra pública COM autenticação COM descricao inválida
     *
     * @throws Exception exceção
     */
    @Test
    @Order(6)
    public void testPostObraPublica_descricaoInvalida_thenStatus400() throws Exception {
        final String descricao = "AKJDHAKJShDLAKhDAKshsahdasd80a9sda8 d90a d09ad09aua0usda09usdfsklçjflskjdflksjdflkjsdlkfjslkdfslkdjfslkjddn0a9und09aunsd0anu0sdAKJDHAKJShDLAKhDAKshsahdasd80a9sda8 d90a d09ad09aua0usda09udn0a9und09aunsd0anu0sdAKJDHAKJShDLAKhDAKshsahdasd80a9sda8 d90a d09ad09aua0usda09udn0a9und09aunsd0anu0sd";
        final MvcResult result = mvc.perform(postWithAuth(URL)
                .content(objectMapper.writeValueAsString(new ObraPublicaDTO(sequence++, descricao, LocalDate.now(), null, CD_RESP))))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        final ObraResponse response = getObraResponse(result);
        assertNotNull(response);

        final String messageError = response.getErrors().get(0).getMessage();
        assertNotNull(messageError);

        assertEquals(messageError, messageService.getMessage("obra.descricao.invalida"));
    }

    /**
     * Teste para inclusão de obra pública SEM autenticação.
     *
     * @throws Exception exceção
     */
    @Test
    @Order(7)
    public void testPostObraPublica_semAutenticacao_thenStatus401() throws Exception {
        mvc.perform(postWithoutAuth(URL)
                .content(objectMapper.writeValueAsString(
                        new ObraPublicaDTO(13, "ObraPublica Post 5", LocalDate.now(), null, "RESP-OB-POST-2"))))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    /**
     * Teste para inclusão de obra pública SEM permissao.
     *
     * @throws Exception
     */
    @Test
    @Order(8)
    public void testPostObraPublica_semPermissao_thenStatus403() throws Exception {
        mvc.perform(defaultBuilder(MockMvcRequestBuilders.post(URL)
                .header("Authorization", getTokenUser1())))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

}