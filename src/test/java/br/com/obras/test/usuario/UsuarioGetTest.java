package br.com.obras.test.usuario;

import br.com.obras.test.AbstractBootTest;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

public class UsuarioGetTest extends AbstractBootTest {

    private static final String URL = V1_OBRAS + "/usuarios";

    /**
     * Teste para lista de usuários COM autenticação.
     *
     * @throws Exception
            */
    @Test
    public void testListaUsuarios_thenStatus200() throws Exception {
        mvc.perform(getWithAuth(URL))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }

    /**
     * Teste para lista de usuários SEM autenticação.
     *
     * @throws Exception
     */
    @Test
    public void testListaUsuarios_semAutenticacao_thenStatus401() throws Exception {
        mvc.perform(getWithoutAuth(URL))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized())
                .andReturn();
    }

}