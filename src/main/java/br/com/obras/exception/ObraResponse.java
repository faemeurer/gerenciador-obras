package br.com.obras.exception;


import br.com.obras.config.ContextProvider;
import br.com.obras.service.MessageService;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Response padrão para aplicação. Desta forma todos os retornos do serviço terão o mesmo conteúdo.
 *  "timestamp" - Define o data e hora que ocorreu
 *  "data" - conteúdo do retorno.
 *  "errors" - lista de mensagens de erros, somente quando houver.
 *
 * @author Rafael Meurer
 */
public class ObraResponse {

    private LocalDateTime timestamp = LocalDateTime.now();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String description =  ((MessageService) ContextProvider.getBean(MessageService.class)).getMessage("sucesso.solicitacao");

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Object data;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<ErrorMessage> errors = new LinkedList();

    public ObraResponse() {
        //
    }

    public ObraResponse(Object data) {
        this.data= data;
    }

    public ObraResponse(String description, ErrorMessage error) {
        this.description = description;
        Optional.ofNullable(error).ifPresent(this.errors::add);
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public List<ErrorMessage> getErrors() {
        return errors;
    }

    public String getDescription() {
        return description;
    }

    public Object getData() {
        return data;
    }

}