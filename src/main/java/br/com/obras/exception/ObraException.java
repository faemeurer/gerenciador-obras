package br.com.obras.exception;

/**
 * Classe de exceção
 *
 * @author Rafael Meurer
 */
public class ObraException extends RuntimeException {

    /**
     * Contrutor default
     *
     * @param message Mensagem
     */
    public ObraException(String message) {
        super(message);
    }

}