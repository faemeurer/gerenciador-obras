package br.com.obras.exception;

import br.com.obras.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Classe responsavel pelas exceções ocorridas na aplicação.
 *
 * @author Rafael Meurer
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageService messageService;

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ObraResponse> handleException(Exception ex){
        final ObraResponse response = new ObraResponse(getErrorMessage(), new ErrorMessage(ex.getMessage()));
        return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ObraException.class)
    public ResponseEntity<ObraResponse> handleException(ObraException ex) {
        final ObraResponse response = new ObraResponse(getErrorMessage(), new ErrorMessage(ex.getMessage()));
        return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        final ObraResponse response = new ObraResponse(getErrorMessage(), null);
        ex.getBindingResult().getFieldErrors().stream().forEach(error ->
                response.getErrors().add(new ErrorMessage(error.getField(), error.getDefaultMessage())));
        ex.getBindingResult().getGlobalErrors().stream().forEach(error ->
                response.getErrors().add(new ErrorMessage(error.getDefaultMessage())));
        return new ResponseEntity<>(response, headers, status);
    }

    private String getErrorMessage() {
        return messageService.getMessage("erro.solicitacao");
    }

}