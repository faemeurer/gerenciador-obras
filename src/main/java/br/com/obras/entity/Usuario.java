package br.com.obras.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
public class Usuario {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String nome;
    private String usuario;
    @JsonIgnore
    private String senha;
    private String email;

    @ManyToMany
    private List<Permissao> permissoes = new ArrayList();

    public Usuario() {

    }

    public Usuario(String nome, String usuario, String senha, String email, Permissao ... permissoes) {
        Optional.ofNullable(nome).ifPresent(this::setNome);
        Optional.ofNullable(usuario).ifPresent(this::setUsuario);
        Optional.ofNullable(senha).ifPresent(this::setSenha);
        Optional.ofNullable(email).ifPresent(this::setEmail);
        Optional.ofNullable(permissoes).map(r -> Stream.of(r).collect(Collectors.toList())).ifPresent(this::setPermissoes);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() { return senha; }

    public void setSenha(String senha) { this.senha = senha; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public List<Permissao> getPermissoes() {
        return permissoes;
    }

    public void setPermissoes(List<Permissao> permissoes) {
        this.permissoes = permissoes;
    }

}