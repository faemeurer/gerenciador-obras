package br.com.obras.entity;

import javax.persistence.Entity;
import java.time.LocalDate;
import java.util.Optional;

/**
 * Entidade Obra Publica
 *
 * @author Rafael Meurer
 */
@Entity
public class ObraPublica extends Obra {

    private LocalDate dataInicio;
    private LocalDate dataFim;

    public ObraPublica() {

    }

    public ObraPublica(Integer numero, String descricao, LocalDate dataInicio, LocalDate dataFim, Responsavel ... responsaveis) {
        super(numero, descricao, responsaveis);
        Optional.ofNullable(dataInicio).ifPresent(this::setDataInicio);
        Optional.ofNullable(dataFim).ifPresent(this::setDataFim);
    }

    public LocalDate getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(LocalDate dataInicio) {
        this.dataInicio = dataInicio;
    }

    public LocalDate getDataFim() {
        return dataFim;
    }

    public void setDataFim(LocalDate dataFim) {
        this.dataFim = dataFim;
    }

}