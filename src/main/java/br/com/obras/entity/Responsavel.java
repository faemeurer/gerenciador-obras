package br.com.obras.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Optional;

/**
 * Entidade Responsável
 *
 * @author Rafael Meurer
 */
@Entity
public class Responsavel {

	@Id
	@JsonIgnore
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, length = 20, unique = true)
	private String codigo;

	@Column(nullable = false)
	private String nome;

	@Column(nullable = false, length = 11)
	private String cpf;

	public Responsavel() {

	}

	public Responsavel(String codigo, String nome, String cpf) {
		Optional.ofNullable(codigo).ifPresent(this::setCodigo);
		Optional.ofNullable(nome).ifPresent(this::setNome);
		Optional.ofNullable(cpf).ifPresent(this::setCpf);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}