package br.com.obras.entity;

import br.com.obras.enums.ZonaEnum;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Optional;

/**
 * Entidade Obra Privada
 *
 * @author Rafael Meurer
 */
@Entity
public class ObraPrivada extends Obra {

    @Enumerated(EnumType.STRING)
    private ZonaEnum zona;
    private Integer areaTotal;

    public ObraPrivada() {

    }

    public ObraPrivada(Integer numero, String descricao, Integer area, ZonaEnum zona, Responsavel ... responsaveis) {
        super(numero, descricao, responsaveis);
        Optional.ofNullable(area).ifPresent(this::setAreaTotal);
        Optional.ofNullable(zona).ifPresent(this::setZona);
    }

    public ZonaEnum getZona() {
        return zona;
    }

    public void setZona(ZonaEnum zona) {
        this.zona = zona;
    }

    public Integer getAreaTotal() {
        return areaTotal;
    }

    public void setAreaTotal(Integer areaTotal) {
        this.areaTotal = areaTotal;
    }

}