package br.com.obras.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Entidade Obra
 *
 * @author Rafael Meurer
 */
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class Obra {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Integer numero;

    @Column(name = "DATA_CADASTRO", columnDefinition = "DATE")
    private LocalDate dataCadastro = LocalDate.now();

    @Column(nullable = false)
    private String descricao;

    @ManyToMany
    private Set<Responsavel> responsaveis = new HashSet();

    public Obra() {

    }

    public Obra(Integer numero, String descricao, Responsavel ... responsaveis) {
        Optional.ofNullable(numero).ifPresent(this::setNumero);
        Optional.ofNullable(descricao).ifPresent(this::setDescricao);
        Optional.ofNullable(responsaveis).map(r -> Stream.of(r).collect(Collectors.toSet())).ifPresent(this::setResponsaveis);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set<Responsavel> getResponsaveis() {
        return responsaveis;
    }

    public void setResponsaveis(Set<Responsavel> responsaveis) {
        this.responsaveis = responsaveis;
    }

}