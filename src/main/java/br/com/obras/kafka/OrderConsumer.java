package br.com.obras.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class OrderConsumer {

    @Autowired
    private ObjectMapper objectMapper;

    @KafkaListener(topics = "${order.topic}", groupId = "${spring.kafka.consumer.group-id}")
    public void consumer(final ConsumerRecord consumerRecord) throws JsonProcessingException {
        System.out.println("Key: " + consumerRecord.key());
        System.out.println("Headers: " + consumerRecord.headers());
        System.out.println("Topic: " + consumerRecord.topic());
        System.out.println("Partion: " + consumerRecord.partition());
        System.out.println("Order: " + objectMapper.writeValueAsString(consumerRecord.value()));
        System.out.println("-------------------------------------------------");
    }

}