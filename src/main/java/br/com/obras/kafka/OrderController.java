package br.com.obras.kafka;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/kafka/orders")
public class OrderController {

    private final OrderProducer orderProducer;

    public OrderController(OrderProducer orderProducer) {
        this.orderProducer = orderProducer;
    }

    @RequestMapping(value = "/sendTypeObject", method = RequestMethod.POST)
    public void sendTypeObject(@RequestBody Order order) {
        orderProducer.sendTypeObject(order);
    }

    @RequestMapping(value = "/sendTypeMessage", method = RequestMethod.POST)
    public void sendTypeMessage(@RequestBody Order order) {
        orderProducer.sendTypeMessage(order);
    }

}