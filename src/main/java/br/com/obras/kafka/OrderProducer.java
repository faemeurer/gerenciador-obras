package br.com.obras.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class OrderProducer {

    @Value("${order.topic}")
    private String orderTopic;

    @Autowired
    private KafkaTemplate kafkaTemplate;

    public void sendTypeObject(final Order order) {
        final String mensageKey = UUID.randomUUID().toString();
        kafkaTemplate.send(orderTopic, mensageKey, order);
    }

    public void sendTypeMessage(final Order order) {
        final String mensageKey = UUID.randomUUID().toString();
        final Message<Order> m = MessageBuilder
                .withPayload(order)
                .setHeader(KafkaHeaders.MESSAGE_KEY, mensageKey)
                .setHeader(KafkaHeaders.TOPIC, "ordertopic")
                .setHeader("EVENT_TYPE", "INSERT")
                .build();

        kafkaTemplate.send(m);
    }

}