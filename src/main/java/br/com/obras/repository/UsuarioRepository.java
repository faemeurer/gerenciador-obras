package br.com.obras.repository;

import br.com.obras.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    /**
     * @see br.com.obras.service.UsuarioService#findByUsuarioAndSenha(String, String)
     *
     * @param usuario Nome de usuário
     * @param senha senha do Usuário
     * @return {@link Usuario}
     */
    Usuario findByUsuarioAndSenha(String usuario, String senha);

}