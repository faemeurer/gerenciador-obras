package br.com.obras.repository;

import br.com.obras.entity.ObraPublica;

/**
 * Classe de acesso ao banco para Obra Pública.
 *
 * @author Rafael Meurer
 */
public interface ObraPublicaRepository extends ObraRepository<ObraPublica> {
	//
}
