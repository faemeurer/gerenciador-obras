package br.com.obras.repository;

import br.com.obras.entity.ObraPrivada;

/**
 * Classe de acesso ao banco para Obra Privada.
 *
 * @author Rafael Meurer
 */
public interface ObraPrivadaRepository extends ObraRepository<ObraPrivada> {
	//
}
