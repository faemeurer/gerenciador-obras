package br.com.obras.repository;

import br.com.obras.controller.ObrasController;
import br.com.obras.entity.Obra;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Classe de acesso ao banco para Obras(pública e privada).
 *
 * @author Rafael Meurer
 */
public interface ObraRepository<T extends Obra> extends JpaRepository<T, Long> {

    /**
     * @see ObrasController#listarObras(Pageable)
     *
     * @param pageable {@link Pageable}
     * @return Lista de Obras (públicas e privadas)
     */
    List<T> getAllByOrderByNumeroDesc(Pageable pageable);

    /**
     * @see ObrasController#listarObrasResponsavel(String, Pageable)
     *
     * @param codigo Código do Responsável
     * @param pageable {@link Pageable}
     * @return Lista de Obras (públicas e privadas)
     */
    @Query("FROM Obra o JOIN o.responsaveis r WHERE r.codigo = :codigo ORDER BY o.numero DESC")
    List<T> findAllByResponsavel(@Param("codigo") String codigo, Pageable pageable);

    /**
     * Recupera a obra pelo número
     *
     * @param numero Número do obra
     * @return {@link Obra}
     */
    T getByNumero(Integer numero);

    /**
     * Verifica se ja existe alguma obra para o número
     * @param numero Número da obra.
     * @return true se existir
     */
    boolean existsByNumero(Integer numero);
}