package br.com.obras.repository;

import br.com.obras.entity.Responsavel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Classe de acesso ao banco para Resposável.
 *
 * @author Rafael Meurer
 */
public interface ResponsavelRepository extends JpaRepository<Responsavel, Long> {

    /**
     * Busca o resposável pelo código.
     *
     * @param codigo Código do responsável
     * @return {@link Responsavel}
     */
    Responsavel getByCodigo(String codigo);

    /**
     * Verifica se existe registro para o cpf
     * @param cpf Cpf do responsável
     * @return true se existir
     */
    boolean existsByCpf(String cpf);

    /**
     * Verifica se existe registro para o código
     * @param codigo Códido do responsável
     * @return true se existir
     */
    boolean existsByCodigo(String codigo);

}