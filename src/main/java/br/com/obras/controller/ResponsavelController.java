package br.com.obras.controller;

import br.com.obras.annotations.PageableAndSort;
import br.com.obras.config.Constants;
import br.com.obras.dto.ResponsavelDTO;
import br.com.obras.dto.ResponsavelInsertDTO;
import br.com.obras.entity.Responsavel;
import br.com.obras.exception.ObraResponse;
import br.com.obras.service.ResponsavelService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Size;

/**
 * Controller para Obra Pública.
 *
 * @author Rafael Meurer
 */
@Validated
@RestController
@RequestMapping(path = Constants.V1_OBRAS + "/responsaveis", produces = {MediaType.APPLICATION_JSON_VALUE})
public class ResponsavelController {

	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private ResponsavelService service;

    /**
     * Método POST para salvar/atualizar Responsável
     *
     * @param resp {@link Responsavel}
     * @return {@link ResponseEntity}
     */
	@PostMapping
	public ResponseEntity<ObraResponse> salvar(@Valid @RequestBody final ResponsavelInsertDTO resp) {
		return ResponseEntity.ok(new ObraResponse(service.salvar(modelMapper.map(resp, Responsavel.class))));
	}

	/**
	 * Método PUT para salvar/atualizar Responsável
	 *
	 * @param resp {@link Responsavel}
	 * @return {@link ResponseEntity}
	 */
	@PutMapping
	public ResponseEntity<ObraResponse> alterar(@Valid @RequestBody final ResponsavelDTO resp) {
		return ResponseEntity.ok(new ObraResponse(service.alterar(modelMapper.map(resp, Responsavel.class))));
	}

    /**
     * Método DELETE para remover Resposável.
     * Validação para que seja informado no máximo 20 caracteres.
     *
     * @param codigo Código do Responsável
     * @return {@link ResponseEntity}
     */
	@DeleteMapping(path = "/{codigo}")
	public ResponseEntity<ObraResponse> remover(@PathVariable @Size(max = 20, message = "{responsavel.codigo.invalido}") final String codigo) {
		service.remover(codigo);
		return ResponseEntity.ok(new ObraResponse(codigo));
	}

    /**
     * Método GET para consultar todos os Responsáveis.
     *
	 * @param pageable {@link Pageable}
     * @return Lista de Responsáveis.
     */
	@GetMapping
	@PageableAndSort
	@Cacheable("responsaveis")
	public ResponseEntity<ObraResponse> listar(@PageableDefault Pageable pageable) {
		return ResponseEntity.ok(new ObraResponse(service.getRepository().findAll(pageable).getContent()));
	}

}