package br.com.obras.controller;

import br.com.obras.config.Constants;
import br.com.obras.dto.ObraPrivadaDTO;
import br.com.obras.entity.ObraPrivada;
import br.com.obras.exception.ObraResponse;
import br.com.obras.service.ObraPrivadaService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Controller para Obra Privada.
 *
 * @author Rafael Meurer
 */
@Validated
@RestController
@RequestMapping(path = Constants.V1_OBRAS + "/privadas", produces = {MediaType.APPLICATION_JSON_VALUE})
public class ObraPrivadaController {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ObraPrivadaService service;

    /**
     * Método POST para salvar/atualizar Obra Privada.
     *
     * @param obraPrivada {@link ObraPrivada}
     * @return {@link ResponseEntity}
     */
    @PostMapping
    public ResponseEntity<ObraResponse> salvar(@Valid @RequestBody final ObraPrivadaDTO obraPrivada) {
        return ResponseEntity.ok(new ObraResponse(service.salvar(modelMapper.map(obraPrivada, ObraPrivada.class))));
    }

    /**
     * Método PUT para atualizar Obra Privada.
     *
     * @param obraPrivada {@link ObraPrivada}
     * @return {@link ResponseEntity}
     */
    @PutMapping
    public ResponseEntity<ObraResponse> alterar(@Valid @RequestBody final ObraPrivadaDTO obraPrivada) {
        return ResponseEntity.ok(new ObraResponse(service.alterar(modelMapper.map(obraPrivada, ObraPrivada.class))));
    }

    /**
     * Método DELETE para remover Obra Privada.
     *
     * @param numero Número da Obra Privada.
     * @return {@link ObraPrivada}
     */
    @DeleteMapping(path = "/{numero}")
    public ResponseEntity<ObraResponse> remover(@PathVariable final Integer numero) {
        service.remover(numero);
        return ResponseEntity.ok(new ObraResponse(numero));
    }

}