package br.com.obras.controller;

import br.com.obras.annotations.PageableAndSort;
import br.com.obras.annotations.PageableNotSort;
import br.com.obras.config.Constants;
import br.com.obras.exception.ObraResponse;
import br.com.obras.service.ObraPrivadaService;
import br.com.obras.service.ObraPublicaService;
import br.com.obras.service.ObraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Size;

/**
 * Controller para Obras (Pública e Privada).
 *
 * @author Rafael Meurer
 */
@RestController
@RequestMapping(path = Constants.V1_OBRAS, produces = {MediaType.APPLICATION_JSON_VALUE})
public class ObrasController {

    @Autowired
    private ObraService obraService;
    @Autowired
    private ObraPublicaService obraPublicaService;
    @Autowired
    private ObraPrivadaService obraPrivadaService;

    /**
     * Método GET para consultar todas a obras(Publicas e Providas)
     * ordenadas pelo número da obra do maior para o menor (DESC).
     *
     * @param pageable {@link Pageable}
     * @return Lista de obras.
     */
    @GetMapping
    @PageableNotSort
    @Cacheable("obras")
    public ResponseEntity<ObraResponse> listarObras(@PageableDefault Pageable pageable) {
        return ResponseEntity.ok(new ObraResponse(obraService.getRepository().getAllByOrderByNumeroDesc(pageable)));
    }

    /**
     * Método GET para consultar todas as obras públicas,
     * ordenadas pelo número da obra do maior para o menor (DESC).
     *
     * @param pageable {@link Pageable}
     * @return Lista de obras públicas.
     */
    @GetMapping(path = "/publicas")
    @PageableNotSort
    @Cacheable("obraspublicas")
    public ResponseEntity<ObraResponse> listarObrasPublicas(@PageableDefault Pageable pageable) {
        return ResponseEntity.ok(new ObraResponse(obraPublicaService.getRepository().getAllByOrderByNumeroDesc(pageable)));
    }

    /**
     * Método GET para consultar todas as obras privadas,
     * ordenadas pelo número da obra do maior para o menor (DESC).
     *
     * @param pageable {@link Pageable}
     * @return Lista de obras públicas.
     */
    @GetMapping(path = "/privadas")
    @PageableNotSort
    @Cacheable("obrasprivadas")
    public ResponseEntity<ObraResponse> listarObrasPrivadas(@PageableDefault Pageable pageable) {
        return ResponseEntity.ok(new ObraResponse(obraPrivadaService.getRepository().getAllByOrderByNumeroDesc(pageable)));
    }

    /**
     * Método GET para consultar todas as obras(públicas e privadas)
     * relacionadas a um responsável utilizando o código do responsável
     * como chave de pesquisa.
     *
     * @param codigo Código do Responsável
     * @param pageable {@link Pageable}
     * @return Lista de Obras(públicas e privadas)
     */
    @GetMapping(path = "/responsaveis/{codigo}")
    @PageableAndSort
    @Cacheable("responsaveisByCodigo")
    public ResponseEntity<ObraResponse> listarObrasResponsavel(@PathVariable @Size(max = 20, message = "{responsavel.codigo.invalido}") final String codigo, @PageableDefault Pageable pageable) {
        return ResponseEntity.ok(new ObraResponse(obraService.getRepository().findAllByResponsavel(codigo, pageable)));
    }

}