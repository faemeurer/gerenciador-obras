package br.com.obras.controller;

import br.com.obras.config.Constants;
import br.com.obras.dto.ObraPublicaDTO;
import br.com.obras.entity.ObraPublica;
import br.com.obras.exception.ObraResponse;
import br.com.obras.service.ObraPublicaService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Controller para Obra Pública.
 *
 * @author Rafael Meurer
 */
@Validated
@RestController
@RequestMapping(path = Constants.V1_OBRAS + "/publicas", produces = {MediaType.APPLICATION_JSON_VALUE})
public class ObraPublicaController {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ObraPublicaService service;

    /**
     * Método POST para salvar Obra Pública
     *
     * @param obraPublica {@link ObraPublica}
     * @return {@link ResponseEntity}
     */
    @PostMapping
    public ResponseEntity<ObraResponse> salvar(@Valid @RequestBody final ObraPublicaDTO obraPublica) {
        return ResponseEntity.ok(new ObraResponse(service.salvar(modelMapper.map(obraPublica, ObraPublica.class))));
    }

    /**
     * Método PUT para atualizar Obra Pública
     *
     * @param obraPublica {@link ObraPublica}
     * @return {@link ResponseEntity}
     */
    @PutMapping
    public ResponseEntity<ObraResponse> alterar(@Valid @RequestBody final ObraPublicaDTO obraPublica) {
        return ResponseEntity.ok(new ObraResponse(service.alterar(modelMapper.map(obraPublica, ObraPublica.class))));
    }

    /**
     * Método DELETE para remover Obra Pública
     *
     * @param numero Número da Obra Pública
     * @return {@link ResponseEntity}
     */
    @DeleteMapping(path = "/{numero}")
    public ResponseEntity<ObraResponse> remover(@PathVariable final Integer numero) {
        service.remover(numero);
        return ResponseEntity.ok(new ObraResponse(numero));
    }

}