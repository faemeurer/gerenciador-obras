package br.com.obras.controller;

import br.com.obras.dto.UserDTO;
import br.com.obras.exception.ObraResponse;
import br.com.obras.security.token.UserToken;
import br.com.obras.service.LoginService;
import io.swagger.v3.oas.annotations.security.SecurityRequirements;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/login", produces = {MediaType.APPLICATION_JSON_VALUE})
public class LoginController {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private LoginService service;

    /**
     * Método POST para verificar o usuário e retornar o Token para acesso aos serviços.
     *
     * @param user {@link UserToken}
     * @return {@link ResponseEntity}
     */
    @PostMapping
    @SecurityRequirements //desativar autenticacao no swagger
    public ResponseEntity<ObraResponse> login(@Valid @RequestBody final UserDTO user) {
        return ResponseEntity.ok().body(new ObraResponse(service.getTokenAuthorization(modelMapper.map(user, UserToken.class))));
    }

}