package br.com.obras.controller;

import br.com.obras.config.Constants;
import br.com.obras.exception.ObraResponse;
import br.com.obras.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller para Usuário.
 *
 * @author Rafael Meurer
 */
@Validated
@RestController
@RequestMapping(path = Constants.V1_OBRAS + "/usuarios", produces = {MediaType.APPLICATION_JSON_VALUE})
public class UsuarioController {

	@Autowired
	private UsuarioService service;

    /**
     * Método GET para consultar todos os Usuários.
     *
     * @return Lista de Usuários.
     */
	@GetMapping
	public ResponseEntity<ObraResponse> listar() {
		return ResponseEntity.ok().body(new ObraResponse(service.getRepository().findAll()));
	}

}