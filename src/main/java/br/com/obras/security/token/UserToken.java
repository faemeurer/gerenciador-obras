package br.com.obras.security.token;

import br.com.obras.entity.Permissao;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

public class UserToken {

    private String username;
    private String password;
    private String name;
    private String email;
    private String sessionToken;
    private List<String> profiles = new ArrayList<>();

    public UserToken() {

    }

    public UserToken(String nome, String email, List<Permissao> permissoes) {
        Optional.ofNullable(nome).ifPresent(this::setName);
        Optional.ofNullable(email).ifPresent(this::setEmail);
        Optional.ofNullable(permissoes).map(l -> l.stream().map(p -> "ROLE_" + p.getNome()).collect(Collectors.toList())).ifPresent(this::setProfiles);
        this.setSessionToken(new StringBuilder()
                .append(Instant.now().toEpochMilli())
                .append("-")
                .append(UUID.randomUUID().toString()).toString());
    }

    public String getName() {
        return this.name;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() { return password; }

    public String getEmail() {
        return this.email;
    }

    public String getSessionToken() {
        return this.sessionToken;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) { this.password = password; }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public List<String> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<String> profiles) {
        this.profiles = profiles;
    }

    public int hashCode() {
        int result = 31 + (this.password == null ? 0 : this.password.hashCode());
        return 31 * result + this.username.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (this.getClass() != obj.getClass()) {
            return false;
        } else {
            UserToken other = (UserToken) obj;
            return Objects.equals(this.email, other.email)
                    && Objects.equals(this.username, other.username)
                    && Objects.equals(this.password, other.password);
        }
    }

}