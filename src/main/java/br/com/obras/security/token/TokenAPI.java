package br.com.obras.security.token;

import io.jsonwebtoken.*;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

public final class TokenAPI {

    private static final String SECRET_KEY = "naoconsigolembrar";
    private static final byte[] KEY_BYTES;

    private TokenAPI() {
    }

    public static String createBearerSchema(UserToken usuario) {
        return createBearerSchema(usuario, true);
    }

    public static String createBearerSchema(UserToken usuario, boolean expire) {
        return "Bearer " + generateAuthToken(usuario, expire);
    }

    public static String generateAuthToken(UserToken user) {
        return generateAuthToken(user, true);
    }

    public static String generateAuthToken(UserToken user, boolean expire) {
        String accessToken = null;

        try {
            Claims claims = Jwts.claims();
            if (expire) {
                claims.setExpiration(Date.from(LocalDateTime.now().plusHours(8L).atZone(ZoneId.systemDefault()).toInstant()));
            }
            claims.setSubject(user.getUsername());
            claims.setIssuer(user.getSessionToken());

            claims.put("nam", user.getName());
            claims.put("eml", user.getEmail());
            claims.put("prf", user.getProfiles().toArray());
            accessToken = Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS256, KEY_BYTES).compact();
        } catch (Exception var4) {
            //Log.getLogger(TokenAPI.class).error("Erro ao gerar token", var4);
        }

        return accessToken;
    }

    public static UserToken loadUserInformation(String authToken) throws JwtException {
        if (!StringUtils.hasText(authToken)) {
            return null;
        } else {
            UserToken builder = new UserToken();
            try {
                Jws<Claims> parsed = Jwts.parser().setSigningKey(KEY_BYTES).parseClaimsJws(authToken);
                Claims body = parsed.getBody();
                parsed.getSignature();

                builder.setUsername(body.getSubject());
                builder.setSessionToken(body.getIssuer());

                body.getExpiration();

                builder.setName(body.get("nam").toString());
                Optional.ofNullable(body.get("eml")).map(Object::toString).ifPresent(builder::setEmail);

                List profiles = (List) body.get("prf");
                Iterator var7 = profiles.iterator();

                while (var7.hasNext()) {
                    Object profile = var7.next();
                    builder.getProfiles().add(profile.toString().toUpperCase(Locale.getDefault()));
                }
            } catch (Exception e) {
                throw new JwtException("Token inválido");
            }
            return builder;
        }
    }

    public static UserToken getUserFromRequest(HttpServletRequest request) throws JwtException {
        String token = getAuthorization(request);
        return loadUserInformation(getAuthToken(token));
    }

    public static String getAuthorization(HttpServletRequest httpRequest) throws JwtException {
        String token = httpRequest.getHeader("Authorization");
        if (token != null) {
            return token;
        } else {
            Cookie[] cookies = httpRequest.getCookies();
            if (cookies != null) {
                Cookie[] var3 = cookies;
                int var4 = cookies.length;
                for (int var5 = 0; var5 < var4; ++var5) {
                    Cookie cookie = var3[var5];
                    if ("Authorization".equals(cookie.getName())) {
                        return cookie.getValue();
                    }
                }
            }
            return null;
        }
    }

    private static String getAuthToken(String authHeader) {
        if (StringUtils.hasText(authHeader)) {
            String value = authHeader.replaceAll("\"", "");
            if (value.startsWith("Bearer ") && value.length() > "Bearer ".length()) {
                return value.substring("Bearer ".length());
            }
        }
        return null;
    }

    static {
        KEY_BYTES = Base64Utils.encode(SECRET_KEY.getBytes(StandardCharsets.UTF_8));
    }

}