package br.com.obras.security;

import br.com.obras.security.token.TokenAPI;
import br.com.obras.security.token.UserToken;
import io.jsonwebtoken.JwtException;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class AuthorizationFilter implements Filter {

    private static final Map<String, Long> CACHE = new ConcurrentHashMap<>();
    private static final long CACHE_LIMIT = 900000;

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) req;
        final HttpServletResponse response = (HttpServletResponse) resp;

        SecurityContextHolder.clearContext();

        try {
            final UserToken user = TokenAPI.getUserFromRequest(request);
            if (user != null) {
                auth(user);
            }
        } catch (JwtException e) {
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.setCharacterEncoding("UTF-8");
            response.getOutputStream().println("[\"" + e.getMessage() + "\" ]");
        } finally {
            filterChain.doFilter(req, resp);
        }
    }

    private void auth(UserToken user) throws JwtException {
        final String sessionToken = user.getSessionToken();
        final Long lastTimeCalled = CACHE.get(sessionToken);
        final Long time = System.currentTimeMillis() - (lastTimeCalled == null ? 0 : lastTimeCalled);
        if (lastTimeCalled == null) {
            CACHE.put(sessionToken, System.currentTimeMillis());
        } else if (time > CACHE_LIMIT) {
            throw new JwtException("session.expired");
        }
        SecurityContextHolder.getContext().setAuthentication(new UserAuthentication(user));
    }

}