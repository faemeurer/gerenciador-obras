package br.com.obras.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * DTO para Obra
 *
 * @author Rafael Meurer
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class ObraDTO {

    @NotNull(message = "{campo.obrigatorio}")
    private Integer numero;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private LocalDate dataCadastro = LocalDate.now();

    @NotEmpty(message = "{campo.obrigatorio}")
    @Size(max = 255, message = "{obra.descricao.invalida}")
    private String descricao;

    @NotEmpty(message = "{campo.obrigatorio}")
    private Set<ObraResponsavelDTO> responsaveis = new HashSet();

    public ObraDTO() {
        //
    }

    public ObraDTO(Integer numero, String descricao, Set<ObraResponsavelDTO> responsaveis) {
        Optional.ofNullable(numero).ifPresent(this::setNumero);
        Optional.ofNullable(descricao).ifPresent(this::setDescricao);
        Optional.ofNullable(responsaveis).ifPresent(this::setResponsaveis);
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set<ObraResponsavelDTO> getResponsaveis() {
        return responsaveis;
    }

    public void setResponsaveis(Set<ObraResponsavelDTO> responsaveis) {
        this.responsaveis = responsaveis;
    }

}