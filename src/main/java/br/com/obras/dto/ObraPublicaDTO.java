package br.com.obras.dto;

import java.time.LocalDate;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * DTO para Obra Publica
 *
 * @author Rafael Meurer
 */
public class ObraPublicaDTO extends ObraDTO {

    private LocalDate dataInicio;
    private LocalDate dataFim;

    public ObraPublicaDTO() {
        //
    }

    public ObraPublicaDTO(Integer numero, String descricao, LocalDate dataInicio, LocalDate dataFim, String ... codigosResp) {
        super(numero, descricao, Stream.of(codigosResp).map(ObraResponsavelDTO::new).collect(Collectors.toSet()));
        Optional.ofNullable(dataInicio).ifPresent(this::setDataInicio);
        Optional.ofNullable(dataFim).ifPresent(this::setDataFim);
    }

    public LocalDate getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(LocalDate dataInicio) {
        this.dataInicio = dataInicio;
    }

    public LocalDate getDataFim() {
        return dataFim;
    }

    public void setDataFim(LocalDate dataFim) {
        this.dataFim = dataFim;
    }

}