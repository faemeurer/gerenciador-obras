package br.com.obras.dto;

import br.com.obras.enums.ZonaEnum;

import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * DTO para Obra Privada
 *
 * @author Rafael Meurer
 */
public class ObraPrivadaDTO extends ObraDTO {

    @NotNull(message = "{obra.zona.obrigatorio}")
    private ZonaEnum zona;
    private Integer areaTotal;

    public ObraPrivadaDTO() {
        //
    }

    public ObraPrivadaDTO(Integer numero, String descricao, ZonaEnum zona, Integer areaTotal, String ... codigosResp) {
        super(numero, descricao, Stream.of(codigosResp).map(ObraResponsavelDTO::new).collect(Collectors.toSet()));
        Optional.ofNullable(zona).ifPresent(this::setZona);
        Optional.ofNullable(areaTotal).ifPresent(this::setAreaTotal);
    }

    public ZonaEnum getZona() {
        return zona;
    }

    public void setZona(ZonaEnum zona) {
        this.zona = zona;
    }

    public Integer getAreaTotal() {
        return areaTotal;
    }

    public void setAreaTotal(Integer areaTotal) {
        this.areaTotal = areaTotal;
    }

}