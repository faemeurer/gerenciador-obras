package br.com.obras.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Optional;

/**
 * DTO para Responsável
 *
 * @author Rafael Meurer
 */
public class ResponsavelDTO {

	@NotEmpty(message = "{campo.obrigatorio}")
	@Size(max = 20, message = "{responsavel.codigo.invalido}")
	private String codigo;

	@NotEmpty(message = "{campo.obrigatorio}")
	@Size(max = 255, message = "{responsavel.nome.invalido}")
	private String nome;

	public ResponsavelDTO() {
		//
	}

	public ResponsavelDTO(String codigo, String nome) {
		Optional.ofNullable(codigo).ifPresent(this::setCodigo);
		Optional.ofNullable(nome).ifPresent(this::setNome);
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}