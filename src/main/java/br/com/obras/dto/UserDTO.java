package br.com.obras.dto;

import javax.validation.constraints.NotEmpty;
import java.util.Optional;

public class UserDTO {

    @NotEmpty(message = "{campo.obrigatorio}")
    private String username;
    @NotEmpty(message = "{campo.obrigatorio}")
    private String password;

    public UserDTO() {
        //
    }

    public UserDTO(String username, String password) {
        Optional.ofNullable(username).ifPresent(this::setUsername);
        Optional.ofNullable(password).ifPresent(this::setPassword);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}