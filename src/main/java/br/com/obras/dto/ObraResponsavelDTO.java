package br.com.obras.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Optional;

public class ObraResponsavelDTO {

    @NotEmpty(message = "{campo.obrigatorio}")
    @Size(max = 20, message = "{responsavel.codigo.invalido}")
    private String codigo;

    public ObraResponsavelDTO() {
        //
    }

    public ObraResponsavelDTO(String codigo) {
        Optional.ofNullable(codigo).ifPresent(this::setCodigo);
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

}