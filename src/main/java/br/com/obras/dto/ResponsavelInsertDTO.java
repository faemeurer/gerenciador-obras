package br.com.obras.dto;

import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Optional;

public class ResponsavelInsertDTO extends ResponsavelDTO{

    @CPF
    @NotEmpty(message = "{campo.obrigatorio}")
    @Size(min = 11, max = 11, message = "{responsavel.cpf.invalido}")
    private String cpf;

    public ResponsavelInsertDTO() {
        //
    }

    public ResponsavelInsertDTO(String codigo, String nome, String cpf) {
        super(codigo, nome);
        Optional.ofNullable(cpf).ifPresent(this::setCpf);
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

}