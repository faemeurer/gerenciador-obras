package br.com.obras.config;

public final class Constants {

    public static final String V1 = "/v1";
    public static final String V1_OBRAS = V1 + "/obras";

}
