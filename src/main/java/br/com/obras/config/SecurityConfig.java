package br.com.obras.config;

import br.com.obras.enums.ProfilesEnum;
import br.com.obras.security.AuthorizationEntryPoint;
import br.com.obras.security.AuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String[] WHITELIST = new String[] {
            "/swagger-ui/**",
            "/login/**",
            "/kafka/**",
            "/actuator/**"};

    @Autowired
    private AuthorizationEntryPoint authorizationEntryPoint;

    @Autowired
    private AuthorizationFilter authorizationFilter;

    @Value("${spring.h2.console.path}")
    private String pathH2;

    @Value("${springdoc.swagger-ui.path}")
    private String swaggerUiPath;

    @Value("${springdoc.api-docs.path}")
    private String swaggerDocsPath;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.exceptionHandling().and()//
            .anonymous().and()//
            .servletApi().and()//
            .headers().frameOptions().disable().and()
            .csrf().disable().headers().frameOptions().disable().and()
            .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers(pathH2+"/**").permitAll()
                .antMatchers(swaggerUiPath+"/**").permitAll()//
                .antMatchers(swaggerDocsPath+"/**").permitAll()//
                .antMatchers(WHITELIST).permitAll()//
                //permissao por verbo http
                .antMatchers(HttpMethod.GET).hasRole(ProfilesEnum.READ.name())
                .antMatchers(HttpMethod.PUT).hasRole(ProfilesEnum.UPDATE.name())
                .antMatchers(HttpMethod.POST).hasRole(ProfilesEnum.SAVE.name())
                .antMatchers(HttpMethod.DELETE).hasRole(ProfilesEnum.DELETE.name())
            .anyRequest().authenticated().and()
            .exceptionHandling().authenticationEntryPoint(authorizationEntryPoint).and()//
            .addFilterBefore(authorizationFilter, UsernamePasswordAuthenticationFilter.class);
    }

}