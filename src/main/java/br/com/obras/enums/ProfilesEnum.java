package br.com.obras.enums;

public enum ProfilesEnum {

    SAVE,
    UPDATE,
    DELETE,
    READ;

}