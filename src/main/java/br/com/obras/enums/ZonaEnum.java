package br.com.obras.enums;

/**
 * Enumerador para classificação de Zona urbana e rural
 *
 * @author Rafael Meurer
 */
public enum ZonaEnum {

    RURAL,
    URBANA;

}