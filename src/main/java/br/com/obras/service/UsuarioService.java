package br.com.obras.service;

import br.com.obras.entity.Usuario;
import br.com.obras.exception.ObraException;
import br.com.obras.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository repository;
    @Autowired
    private MessageService messageService;

    public UsuarioRepository getRepository() {
        return repository;
    }

    public Usuario verificarUsuario(String username, String password) {
        return Optional.ofNullable(repository.findByUsuarioAndSenha(username, password))
                .orElseThrow(() -> new ObraException(messageService.getMessage("login.invalido")));
    }

}