package br.com.obras.service;

import br.com.obras.repository.ObraPrivadaRepository;
import br.com.obras.repository.ObraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Classe de negócio para Obra Privada.
 *
 * @author Rafael Meurer
 */
@Service
public class ObraPrivadaService extends AbstractObra {

    @Autowired
    private ObraPrivadaRepository repository;

    /**
     * Get repository
     *
     * @return {@link ObraPrivadaRepository}
     */
    @Override
    public ObraRepository getRepository() {
    	return repository;
    }

}