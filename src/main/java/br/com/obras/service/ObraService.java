package br.com.obras.service;

import br.com.obras.entity.Obra;
import br.com.obras.repository.ObraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ObraService {

    @Autowired
    private ObraRepository<Obra> repository;

    public ObraRepository<Obra> getRepository() {
        return repository;
    }

}