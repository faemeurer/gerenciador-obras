package br.com.obras.service;

import br.com.obras.repository.ObraPublicaRepository;
import br.com.obras.repository.ObraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Classe de negócio para Obra Pública
 *
 * @author Rafael Meurer
 */
@Service
public class ObraPublicaService extends AbstractObra {

    @Autowired
    private ObraPublicaRepository repository;

    /**
     * Get repository
     *
     * @return {@link ObraPublicaRepository}
     */
    @Override
    public ObraRepository getRepository() {
    	return repository;
    }

}