package br.com.obras.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

@Service
public class MessageService {

    @Autowired
    private MessageSource messageSource;

    public String getMessage(String key) {
        return messageSource.getMessage(key, null, null);
    }

    public String getMessage(String key, Object ... params) {
        return messageSource.getMessage(key, params, null);
    }

}