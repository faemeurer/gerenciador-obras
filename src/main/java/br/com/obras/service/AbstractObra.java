package br.com.obras.service;

import br.com.obras.entity.Obra;
import br.com.obras.entity.Responsavel;
import br.com.obras.exception.ObraException;
import br.com.obras.repository.ObraRepository;
import br.com.obras.repository.ResponsavelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Classe abstrata para o serviço de obras(pública e privada).
 *
 * @author Rafael Meurer
 */
@CacheConfig(cacheNames = {"obrasprivadas", "obraspublicas", "obras"})
public abstract class AbstractObra {

    @Autowired
    protected MessageService message;
    @Autowired
    protected ResponsavelRepository responsavelRepository;

    public abstract ObraRepository<Obra> getRepository();

    /**
     * Salva as informções do objeto na base dados.
     *
     * @param obra {@link Obra}
     * @return {@link Obra}
     */
    @CacheEvict(cacheNames = {"obrasprivadas", "obraspublicas", "obras"}, allEntries = true)
    public Obra salvar(Obra obra) {
        if (getRepository().existsByNumero(obra.getNumero()))
            throw new ObraException(message.getMessage("obra.numero.existente"));
        return salvarOuAlterar(obra);
    }

    /**
     * Altera as informções do objeto na base dados.
     *
     * @param obra {@link Obra}
     * @return {@link Obra}
     */
    @CachePut(cacheNames = {"obrasprivadas", "obraspublicas", "obras"}, key = "#obra.numero")
    public Obra alterar(Obra obra) {
        //se já exisitir uma obra com o número, atribui o id para realizar alteração do objeto
        Optional.ofNullable(getRepository().getByNumero(obra.getNumero())).map(Obra::getId).ifPresent(obra::setId);
        Optional.ofNullable(obra.getId()).orElseThrow(() -> new ObraException(message.getMessage("obra.nao.existe")));
        return salvarOuAlterar(obra);
    }

    private Obra salvarOuAlterar(Obra obra) {
        final Set<Responsavel> responsaveis = new HashSet<>();
        for (final Responsavel resp : obra.getResponsaveis()) {
            if (Objects.isNull(resp.getCodigo()) || resp.getCodigo().isEmpty())
                throw new ObraException(message.getMessage("responsavel.codigo.nao.informado"));
            //localiza pelo codigo para atribuir o id do responsavel para o objeto
            final Responsavel responsavel = responsavelRepository.getByCodigo(resp.getCodigo());
            Optional.ofNullable(responsavel).map(Objects::isNull).orElseThrow(() ->
                    new ObraException(message.getMessage("responsavel.nao.encontrado", resp.getCodigo())));
            responsaveis.add(responsavel);
        }
        obra.setResponsaveis(responsaveis);
        return getRepository().save(obra);
    }

    /**
     * Remove da base de dados se o número da obra existir.
     *
     * @param numero Número da obra
     */
    @CacheEvict(cacheNames = {"obrasprivadas", "obraspublicas", "obras"}, allEntries = true)
    public void remover(Integer numero) {
        final Obra obra = getRepository().getByNumero(numero);
        Optional.ofNullable(obra).orElseThrow(() -> new ObraException(message.getMessage("obra.nao.existe")));
        getRepository().delete(obra);
    }

}