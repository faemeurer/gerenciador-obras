package br.com.obras.service;

import br.com.obras.entity.Usuario;
import br.com.obras.security.token.TokenAPI;
import br.com.obras.security.token.UserToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    @Autowired
    private UsuarioService usuarioService;

    public String getTokenAuthorization(UserToken usuarioLogin) {
        final Usuario usuarioEntity = usuarioService.verificarUsuario(usuarioLogin.getUsername(), usuarioLogin.getPassword());
        return TokenAPI.createBearerSchema(new UserToken(usuarioEntity.getNome(), usuarioEntity.getEmail(), usuarioEntity.getPermissoes()), true);
    }

}