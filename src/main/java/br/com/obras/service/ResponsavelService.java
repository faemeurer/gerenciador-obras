package br.com.obras.service;

import br.com.obras.entity.Responsavel;
import br.com.obras.exception.ObraException;
import br.com.obras.repository.ResponsavelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Classe de negócio para o Responsável.
 *
 * @author Rafael Meurer
 */
@Service
@CacheConfig(cacheNames = {"responsaveis", "responsaveisByCodigo"})
public class ResponsavelService {

    @Autowired
    private MessageService message;

    @Autowired
    private ResponsavelRepository repository;

    /**
     * Get repository
     *
     * @return {@link ResponsavelRepository}
     */
    public ResponsavelRepository getRepository() {
        return repository;
    }

    /**
     * Salva as informções do objeto na base dados.
     *
     * @param resp {@link Responsavel}
     */
    @CacheEvict(cacheNames = {"responsaveis", "responsaveisByCodigo"}, allEntries = true)
    public Responsavel salvar(Responsavel resp) {
        //recupera o código e atribui o uppercase para padronizar
        Optional.of(resp.getCodigo()).map(String::toUpperCase).ifPresent(resp::setCodigo);
        //verifica se tem codigo existente
        if (repository.existsByCodigo(resp.getCodigo()))
            throw new ObraException(message.getMessage("responsavel.codigo.existente"));
        //verifica se tem cpf existente
        if (repository.existsByCpf(resp.getCpf()))
            throw new ObraException(message.getMessage("responsavel.cpf.existente"));
        //salva no banco
        return repository.save(resp);
    }

    /**
     * Altera as informções do objeto na base dados.
     *
     * @param resp {@link Responsavel}
     */
    @CacheEvict(cacheNames = {"responsaveis", "responsaveisByCodigo"}, allEntries = true)
    public Responsavel alterar(Responsavel resp) {
        //recupera o código e atribui o uppercase para padronizar
        Optional.of(resp.getCodigo()).map(String::toUpperCase).ifPresent(resp::setCodigo);
        //verifica se já existe para atribuir o id e cpf para o objeto
        Optional.ofNullable(repository.getByCodigo(resp.getCodigo())).ifPresent(r -> {
            resp.setId(r.getId());
            resp.setCpf(r.getCpf()); //cpf nunca altera
        });
        Optional.ofNullable(resp.getId())
                .orElseThrow(() -> new ObraException(message.getMessage("responsavel.nao.cadastrado")));
        //salva no banco
        return repository.save(resp);
    }

    /**
     * Remove da base de dados se o código do responsável existir.
     *
     * @param codigo Código do responsável
     */
    @CacheEvict(cacheNames = {"responsaveis", "responsaveisByCodigo"}, allEntries = true)
    public void remover(String codigo) {
        //verifica se o código existe
        final Responsavel resp = repository.getByCodigo(codigo.toUpperCase());
        //se não existir, retorna false
        Optional.ofNullable(resp)
                .orElseThrow(() -> new ObraException(message.getMessage("responsavel.nao.existe")));
        repository.delete(resp);
    }

}