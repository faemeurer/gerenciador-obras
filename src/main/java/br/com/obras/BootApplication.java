package br.com.obras;

import br.com.obras.entity.*;
import br.com.obras.enums.ZonaEnum;
import br.com.obras.repository.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;

/**
 * Classe de configuração do Spring Boot
 *
 * @author Rafael Meurer
 */
@SpringBootApplication
public class BootApplication {

	@Autowired
	private UsuarioRepository usuarioRepository;
	@Autowired
	private PermissaoRepository permissaoRepository;
	@Autowired
	private ResponsavelRepository responsavelRepository;
	@Autowired
	private ObraPublicaRepository obraPublicaRepository;
	@Autowired
	private ObraPrivadaRepository obraPrivadaRepository;

	public static void main(String[] args) {
		SpringApplication.run(BootApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Bean
    public CommandLineRunner runCommandLine() {
        return (x -> {
			final Permissao read = permissaoRepository.save(new Permissao(1L, "READ"));
			final Permissao save = permissaoRepository.save(new Permissao(2L, "SAVE"));
			final Permissao delete = permissaoRepository.save(new Permissao(3L, "DELETE"));
			final Permissao update = permissaoRepository.save(new Permissao(4L, "UPDATE"));

			usuarioRepository.save(new Usuario("Usuário administrador", "admin", "admin123", "admin@teste.com.br", read, save, delete, update));
			usuarioRepository.save(new Usuario("Usuário 1", "user1", "user123", "usuario1@teste.com.br", read));

			final Responsavel resp_1 = responsavelRepository.save(new Responsavel("RESP_0001", "Responsável 0001", "08847945046"));
			final Responsavel resp_2 = responsavelRepository.save(new Responsavel("RESP_0002", "Responsável 0002", "66036100000"));
			final Responsavel resp_3 = responsavelRepository.save(new Responsavel("RESP_0003", "Responsável 0003", "80716949083"));

			obraPublicaRepository.save(new ObraPublica(1, "Obra Pública 01", LocalDate.now(), LocalDate.now().plusDays(30), resp_1, resp_2));
			obraPublicaRepository.save(new ObraPublica(2, "Obra Pública 02", LocalDate.now(), null, resp_3));

			obraPrivadaRepository.save(new ObraPrivada(1, "Obra Privada 01", 10, ZonaEnum.RURAL, resp_1, resp_2));
			obraPrivadaRepository.save(new ObraPrivada(2, "Obra Privada 02", 20, ZonaEnum.URBANA, resp_3));
        });
    }

}